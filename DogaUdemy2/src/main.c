/* Includes */
#include "stm32f4xx.h"
#include <stdint.h>
#include <stdio.h>
/* Private macro */
/* Private variables */
/* Private function prototypes */
/* Private functions */

/**
**===========================================================================
**
**  Abstract: main program
**
**===========================================================================
*/


#define AH1B_EN 0x40023830;
#define GPIOA_MODER 0x40020000;
#define GPIOA_OUT 0x40020014
#define GPIOA_IDR 0x40020010
void delayMs(int n);
int main(void)
{
	uint32_t volatile *const pClkPeripEnable = (uint32_t*) AH1B_EN;
	uint32_t volatile *const pPortAModeReg = (uint32_t*) GPIOA_MODER;
	uint32_t volatile *const pPortAOutReg = (uint32_t*) GPIOA_OUT;
	uint32_t const volatile *const pPortAIdrReg = (uint32_t*) GPIOA_IDR;


	*pClkPeripEnable |= (1 << 0);
	//Moder 5 is set to GPIO output and Moder 6 is cleared for input mode
	*pPortAModeReg &= ~(3 << 10);
	*pPortAModeReg |= (1 << 10);
	//printf("hello");
	while(1){
		//printf("HELLO\n");
		uint32_t pinStatus = (uint32_t)(*pPortAIdrReg & (1<<6));
		if(pinStatus){
			delayMs(50);
			*pPortAOutReg |= (1 << 5);
			printf("LED ON\n");
			delayMs(150);

		}else{
			delayMs(50);
			*pPortAOutReg &= ~(1 << 5);

		}
	}

}

void delayMs(int n){
	int i;
	for(; n>0 ; n--)
		for(i=0;i<3195;i++);
}
