#include "stm32f4xx.h"

int main(void)
{
  RCC->AHB1ENR |=2;

  GPIOB->MODER &= ~0x00000CC0;
  GPIOB->MODER = (1<<(5*2)) |  (1<<(3*2));

  RCC->APB1ENR |=1;
  TIM2->PSC =1600-1;
  TIM2->ARR=10000-1;
  TIM2->CNT=0;
  TIM2->CR1 =1;

  while (1)
  {
	GPIOB->ODR = 1<<5;
	while(!(TIM2->SR & 1)) {}
	TIM2->SR &= ~1;
	GPIOB->ODR = 1<<3;
	while(!(TIM2->SR & 1)) {}
	TIM2->SR &= ~1;
  }
}
