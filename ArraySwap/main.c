/*
 * main.c
 *
 *  Created on: 22 Kas 2020
 *      Author: user
 */

#include <stdio.h>
#include <stdint.h>



int main(void){


	uint32_t size_array_1, size_array_2;
	printf("Please enter size of array1 and array 2 respectively \n");
	scanf("%u %u", &size_array_1, &size_array_2);
	int32_t array1[size_array_1];
	int32_t array2[size_array_2];

	for(int i = 0 ; i < size_array_1;i++){
		printf("Element %d of first array \n",i);
		scanf("%d",&array1[i]);
	}

	for(int i = 0 ; i < size_array_2;i++){
		printf("Element %d of second array \n",i);
		scanf("%d",&array2[i]);
	}

	for(int i = 0 ; i < size_array_1;i++){
		printf("Element %d of first array %d \n",i,array1[i]);

	}
	getchar();
	return 0;

}
