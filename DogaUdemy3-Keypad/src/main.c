/* Includes */
#include "stm32f4xx.h"
#include <stdint.h>
#include <stdio.h>
/* Private macro */
/* Private variables */
/* Private function prototypes */
/* Private functions */

/**
**===========================================================================
**
**  Abstract: main program
**
**===========================================================================
*/


#define AH1B_EN 0x40023830;
#define GPIOA_MODER 0x40020000;
#define GPIOA_OUT 0x40020014
#define GPIOA_IDR 0x40020010
#define GPIOA_PULL_UP 0x4002000C

void delayMs(int n);
int main(void)
{
	uint32_t volatile *const pClkPeripEnable = (uint32_t*) AH1B_EN;
	uint32_t volatile *const pPortAModeReg = (uint32_t*) GPIOA_MODER;
	uint32_t volatile *const pPortAOutReg = (uint32_t*) GPIOA_OUT;
	uint32_t const volatile *const pPortAIdrReg = (uint32_t*) GPIOA_IDR;
	uint32_t volatile *const pPortAPullUpReg = (uint32_t*) GPIOA_PULL_UP;


	*pClkPeripEnable |= (1 << 0);
	//Moder 8-9 is set to GPIO output and Moder 5-6-7 is cleared for input mode
	*pPortAModeReg &= ~(0xFF);
	*pPortAModeReg |= (0x05 << 16);

	//internal pull up resistors
	//*pPortAPullUpReg &= ~(0xFFFFFFFF);
	*pPortAPullUpReg |= (0x15 << 10);


	while(1){

		//make all rows HIGH pa8-pa9
		*pPortAOutReg |= 3 << 8 ;
		delayMs(50);
		// R1 (PA8) low
		*pPortAOutReg &= ~(1 << 8) ;
		delayMs(50);

		// check the column C1 (PA7);
		if(!(*pPortAIdrReg & (1<<7))){
			delayMs(100);
			printf("1\n");
		}
		// check the column C2 (PA6);
		else if(!(*pPortAIdrReg & (1<<6))){
			delayMs(100);
				printf("2\n");
			}
		// check the column C3 (PA5);
		else if(!(*pPortAIdrReg & (1<<5))){
			delayMs(100);
				printf("3\n");
			}
		//make all rows HIGH
		*pPortAOutReg |= 3 << 8 ;
		delayMs(50);
		// R2 (PA9) low
		*pPortAOutReg &= ~(1 << 9) ;
		delayMs(50);

		// check the column C1 (PA7);
		if(!(*pPortAIdrReg & (1<<7))){
			delayMs(100);
			printf("4\n");
		}
		// check the column C2 (PA6);
		else if(!(*pPortAIdrReg & (1<<6))){
			delayMs(100);
				printf("5\n");
			}
		// check the column C3 (PA5);
		else if(!(*pPortAIdrReg & (1<<5))){
			delayMs(100);
				printf("6\n");
			}

	}

}

void delayMs(int n){
	int i;
	for(; n>0 ; n--)
		for(i=0;i<3195;i++);
}
