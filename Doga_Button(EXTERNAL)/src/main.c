#include "stm32f4xx.h"


int main(void)
{

  RCC->AHB1ENR =0x00000003;

  GPIOA->MODER &=~0x000F0000;

  GPIOB->MODER &=~0x00000CC0;
  GPIOB->MODER =(1<<(5*2)) | (1<<(3*2));


  while (1)
  {
	  if((GPIOA->IDR & 0x0200))
	  {
		  GPIOB->BSRRH = 1<<3;
		  GPIOB->BSRRL = 1<<5;   //green
	  }
	  else if((GPIOA->IDR & 0x0100))
	  {
		  GPIOB->BSRRH = 1<<5;   //red
		  GPIOB->BSRRL = 1<<3;

	  }
	  else
	  {
		  GPIOB->BSRRH = 1<<3;
		  GPIOB->BSRRH = 1<<5;
	  }


  }
}
