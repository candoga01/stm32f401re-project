/*
 * stm32f401re.h
 *
 *  Created on: 26 Kas 2020
 *      Author: user
 */

#ifndef STM32F401RE_H_
#define STM32F401RE_H_
#include <stdint.h>
/*
 *****base addresses of FLASH , SRAM , ROM ****
 */
#define FLASH_BASEADDR	(0x20000000U)	/* Flash Memory */
#define SRAM1_BASEADDR	(0x08000000U)   /* Main internal SRAM */
#define ROM_BASEADDR	(0x1FFF0000U)	/* System Memory */
/*
 *****base addresses of Peripheral Busses ****
 */
#define PERIPH_BASE			(0x40000000U)
#define APB1PERIPH_BASE		PERIPH_BASE
#define APB2PERIPH_BASE		(0x40010000U)
#define AHB1PERIPH_BASE		(0x40020000U)
#define AHB2PERIPH_BASE		(0x50000000U)

/*
 * Base addresses of peripherals which are hanging on AHB1 Bus *
 */
#define GPIOA_BASEADDR		(AHB1PERIPH_BASE + 0x0000)
#define GPIOB_BASEADDR		(AHB1PERIPH_BASE + 0x0400)
#define GPIOC_BASEADDR		(AHB1PERIPH_BASE + 0x0800)
#define GPIOD_BASEADDR		(AHB1PERIPH_BASE + 0x0C00)
#define GPIOE_BASEADDR		(AHB1PERIPH_BASE + 0x1000)
#define GPIOH_BASEADDR		(AHB1PERIPH_BASE + 0x1C00)
#define RCC_BASEADDR 		(AHB1PERIPH_BASE + 0x3800)
/*
 * Base addresses of peripherals which are hanging on APB1 Bus *
 */
#define I2C1_BASEADDR 		(APB1PERIPH_BASE + 0x5400)
#define I2C2_BASEADDR 		(APB1PERIPH_BASE + 0x5800)
#define I2C3_BASEADDR 		(APB1PERIPH_BASE + 0x5C00)
#define SPI2_BASEADDR 		(APB1PERIPH_BASE + 0x3800)
#define SPI3_BASEADDR 		(APB1PERIPH_BASE + 0x3C00)
#define USART2_BASEADDR		(APB1PERIPH_BASE + 0x4400)
/*
 * Base addresses of peripherals which are hanging on APB2 Bus *
 */
#define USART1_BASEADDR		(APB2PERIPH_BASE + 0x1000)
#define USART6_BASEADDR		(APB2PERIPH_BASE + 0x1400)
#define SPI1_BASEADDR		(APB2PERIPH_BASE + 0x3000)
#define EXTI_BASEADDR		(APB2PERIPH_BASE + 0x3C00)
#define SYSCFG_BASEADDR		(APB2PERIPH_BASE + 0x3800)


/************************STM32F401xD/E**************************
 ************ PERIPHERAL REGISTER STRUCTURE FOR GPIO************
 **************************************************************/
typedef struct {

	volatile uint32_t  MODER;	/*GPIO port mode register 				Address offset: 0x00 */
	volatile uint32_t  OTYPER;	/*GPIO port output type register  		Address offset: 0x04 */
	volatile uint32_t  OSPEEDR;	/*GPIO port output speed register   	Address offset: 0x08 */
	volatile uint32_t  PUPDR;	/*GPIO port pull-up/pull-down register	Address offset: 0x0C */
	volatile uint32_t  IDR;		/*GPIO port input data register			Address offset: 0x10 */
	volatile uint32_t  ODR;		/*GPIO port output data register		Address offset: 0x14 */
	volatile uint32_t  BSRR;		/*GPIO port bit set/reset register		Address offset: 0x18 */
	volatile uint32_t  LCKR;		/*GPIO port configuration lock register	Address offset: 0x1C */
	volatile uint32_t  AFR[2];	/*GPIO alternate function register 		[0] -> LOW -- [1] -> HIGH 		Address offset: 0x20 -- 0x24 */

}GPIO_RegDef_t;

typedef struct {

	volatile uint32_t	CR;			/*RCC clock control register								Address offset: 0x00*/
	volatile uint32_t	PLLCFGR;	/*RCC PLL configuration register							Address offset: 0x04*/
	volatile uint32_t	CFGR;		/*RCC clock configuration register							Address offset: 0x08*/
	volatile uint32_t	CIR;		/*RCC clock interrupt register								Address offset: 0x0C*/
	volatile uint32_t	AHB1RSTR;	/*RCC AHB1 peripheral reset register						Address offset: 0x10*/
	volatile uint32_t	AHB2RSTR;	/*RCC AHB2 peripheral reset register						Address offset: 0x14*/
	volatile uint32_t	RESERVED_0;
	volatile uint32_t	RESERVED_1;
	volatile uint32_t	APB1RSTR;	/*RCC APB1 peripheral reset register						Address offset: 0x20*/
	volatile uint32_t	APB2RSTR;	/*RCC APB2 peripheral reset register						Address offset: 0x24*/
	volatile uint32_t	RESERVED_2;
	volatile uint32_t	RESERVED_3;
	volatile uint32_t	AHB1ENR;	/*RCC AHB1 peripheral clock enable register					Address offset: 0x30*/
	volatile uint32_t	AHB2ENR;	/*RCC AHB2 peripheral clock enable register					Address offset: 0x34*/
	volatile uint32_t	RESERVED_4;
	volatile uint32_t	RESERVED_5;
	volatile uint32_t	APB1ENR;	/*RCC APB1 peripheral clock enable register					Address offset: 0x40*/
	volatile uint32_t	APB2ENR;	/*RCC APB2 peripheral clock enable register					Address offset: 0x44*/
	volatile uint32_t	RESERVED_6;
	volatile uint32_t	RESERVED_7;
	volatile uint32_t	AHB1LPENR;	/*RCC AHB1 peripheral clock enable in low power mode register		Address offset: 0x50*/
	volatile uint32_t	AHB2LPENR;	/*RCC AHB2 peripheral clock enable in low power mode register		Address offset: 0x54*/
	volatile uint32_t	RESERVED_8;
	volatile uint32_t	RESERVED_9;
	volatile uint32_t	APB1LPENR;	/*RCC APB1 peripheral clock enable in low power mode register		Address offset: 0x60*/
	volatile uint32_t	APB2LPENR;	/*RCC APB2 peripheral clock enable in low power mode register		Address offset: 0x64*/
	volatile uint32_t	RESERVED_10;
	volatile uint32_t	RESERVED_11;
	volatile uint32_t	BDCR;		/*RCC Backup domain control register						Address offset: 0x70*/
	volatile uint32_t	CSR;		/*RCC clock control & status register						Address offset: 0x74*/
	volatile uint32_t	RESERVED_12;
	volatile uint32_t	RESERVED_13;
	volatile uint32_t	SSCGR;		/*RCC spread spectrum clock generation register				Address offset: 0x80*/
	volatile uint32_t	PLLI2SCFGR;	/*RCC PLLI2S configuration register							Address offset: 0x84*/
	volatile uint32_t	RESERVED_14;
	volatile uint32_t	DCKCFGR;	/*RCC Dedicated Clocks Configuration Register				Address offset: 0x8C*/

}RCC_RegDef;

/*
 * Peripheral definitions typecasted to xxx_RegDef_t
 * */
#define GPIOA		((GPIO_RegDef_t*)	GPIOA_BASEADDR)
#define GPIOB		((GPIO_RegDef_t*)	GPIOB_BASEADDR)
#define GPIOC		((GPIO_RegDef_t*)	GPIOC_BASEADDR)
#define GPIOD		((GPIO_RegDef_t*)	GPIOD_BASEADDR)
#define GPIOE		((GPIO_RegDef_t*)	GPIOE_BASEADDR)
#define GPIOH		((GPIO_RegDef_t*)	GPIOH_BASEADDR)
#define RCC			((RCC_RegDef*)		RCC_BASEADDR)

/*
 * Clock enable macros for GPIOx Peripherals
 *
 * */
#define GPIOA_PCLK_EN (RCC->AHB1ENR |= (1 << 0))
#define GPIOB_PCLK_EN (RCC->AHB1ENR |= (1 << 1))
#define GPIOC_PCLK_EN (RCC->AHB1ENR |= (1 << 2))
#define GPIOD_PCLK_EN (RCC->AHB1ENR |= (1 << 3))
#define GPIOE_PCLK_EN (RCC->AHB1ENR |= (1 << 4))
#define GPIOH_PCLK_EN (RCC->AHB1ENR |= (1 << 7))
/*
 * Clock enable macros for I2Cx Peripherals
 *
 * */
#define I2C1_PCLK_EN	(RCC->APB1ENR |= (1 << 21))
#define I2C2_PCLK_EN	(RCC->APB1ENR |= (1 << 22))
#define I2C3_PCLK_EN	(RCC->APB1ENR |= (1 << 23))
/*
 * Clock enable macros for SPIx Peripherals
 *
 * */
#define SP11_PCLK_EN	(RCC->APB2ENR |= (1 << 12))
#define SP12_PCLK_EN	(RCC->APB1ENR |= (1 << 14))
#define SP13_PCLK_EN	(RCC->APB1ENR |= (1 << 15))
/*
 * Clock enable macros for USARTx Peripherals
 *
 * */
#define USART1_PCLK_EN	(RCC->APB2ENR |= (1 << 4))
#define USART2_PCLK_EN	(RCC->APB1ENR |= (1 << 17))
#define USART6_PCLK_EN	(RCC->APB2ENR |= (1 << 5))
/*
 * Clock enable macros for SYSCFG Peripherals
 *
 * */
#define SYSCFG_PCLK_EN 	(RCC->APB2ENR |= (1 << 14))
/*
 * Clock disable macros for GPIOx Peripherals
 *
 * */
#define GPIOA_PCLK_DI (RCC->AHB1ENR &= ~(1 << 0))
#define GPIOB_PCLK_DI (RCC->AHB1ENR &= ~(1 << 1))
#define GPIOC_PCLK_DI (RCC->AHB1ENR &= ~(1 << 2))
#define GPIOD_PCLK_DI (RCC->AHB1ENR &= ~(1 << 3))
#define GPIOE_PCLK_DI (RCC->AHB1ENR &= ~(1 << 4))
#define GPIOH_PCLK_DI (RCC->AHB1ENR &= ~(1 << 7))
/*
 * Clock disable macros for I2Cx Peripherals
 *
 * */
#define I2C1_PCLK_DI	(RCC->APB1ENR &= ~(1 << 21))
#define I2C2_PCLK_DI	(RCC->APB1ENR &= ~(1 << 22))
#define I2C3_PCLK_DI	(RCC->APB1ENR &= ~(1 << 23))
/*
 * Clock disable macros for SPIx Peripherals
 *
 * */
#define SP11_PCLK_DI	(RCC->APB2ENR &= ~(1 << 12))
#define SP12_PCLK_DI	(RCC->APB1ENR &= ~(1 << 14))
#define SP13_PCLK_DI	(RCC->APB1ENR &= ~(1 << 15))
/*
 * Clock disable macros for USARTx Peripherals
 *
 * */
#define USART1_PCLK_DI	(RCC->APB2ENR &= ~(1 << 4))
#define USART2_PCLK_DI	(RCC->APB1ENR &= ~(1 << 17))
#define USART6_PCLK_DI	(RCC->APB2ENR &= ~(1 << 5))
/*
 * Clock disable macros for SYSCFG Peripherals
 *
 * */
#define SYSCFG_PCLK_DI 	(RCC->APB2ENR &= ~(1 << 14))

#endif /* STM32F401RE_H_ */
