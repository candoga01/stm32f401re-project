#include "stm32f4xx.h"

int main(void)
{

  RCC->AHB1ENR |=1;
  RCC->APB1ENR |=1;
  GPIOA->MODER &= ~(0x00000C00); //PA5 CLEARED
  GPIOA->MODER |= 1<<(2*5);      //PA5 AS GPIO OUTPUT MODE
  GPIOA->OSPEEDR =0x00000C00;

  TIM2->PSC = 1600-1;
  TIM2->ARR = 10000 - 1;
  TIM2->CNT = 0;
  TIM2->CR1 =1;

  while (1)
  {
	while(!(TIM2->SR & 1)) {}
	TIM2->SR &= ~ 1;

	GPIOA->ODR ^= 1<<5;

  }
}
