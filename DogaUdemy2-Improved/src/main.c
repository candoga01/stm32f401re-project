/* Includes */
#include "stm32f4xx.h"
#include <stdint.h>
#include "main.h"
/* Private macro */
/* Private variables */
/* Private function prototypes */
/* Private functions */

/**
**===========================================================================
**
**  Abstract: main program
**
**===========================================================================
*/

void delayMs(int n);

int main(void)
{

	pClkPeripEnable -> gpioa_en = 1;
	//Moder 5 is set to GPIO output and Moder 6 is cleared for input mode

	pPortAModeReg -> moder_5 = 1;
	pPortAModeReg ->moder_6 = 0;



	while(1){
		uint32_t pinStatus = (uint32_t)(pPortAIdrReg->idr_6);
		if(!(pinStatus == 0)){
			delayMs(500);
			pPortAOdrReg->odr_5 = 1;
		}else{
			delayMs(500);
			pPortAOdrReg->odr_5 = 0 ;
		}
	}

}

void delayMs(int n){
	int i;
	for(; n>0 ; n--)
		for(i=0;i<3195;i++);
}
