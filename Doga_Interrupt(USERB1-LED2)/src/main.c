#include "stm32f4xx.h"

void delayMs();

int main(void)
{
	__disable_irq();

	RCC->AHB1ENR |= 5;
	RCC->APB2ENR |= 0x4000;

	GPIOC->MODER &= ~ 0x0C000000;

	GPIOA->MODER &= ~ 0x00000C00 ;
	GPIOA->MODER |= 1<<(2*5) ;

	SYSCFG->EXTICR[3] &= ~ 0x00F0 ;
	SYSCFG->EXTICR[3] |= 1<<5 ;

	EXTI->IMR |= 0x2000;
	EXTI->FTSR |= 0x2000;

	NVIC_EnableIRQ(EXTI15_10_IRQn);

	__enable_irq();



  while (1)
  {

  }
}
void EXTI15_10_IRQHandler(void)
{
	GPIOA->ODR ^= 1<<5;
    EXTI->PR = 0x2000 ;
}
