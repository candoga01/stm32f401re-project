#include "stm32f4xx.h"

void delayMs(int n);

int main(void)
{

  RCC->AHB1ENR |= 1; // GPIOA
  GPIOA->MODER &= ~(0x00000C00); //PA5 CLEARED
  GPIOA->MODER |= 1<<(2*5);      //PA5 AS GPIO OUTPUT MODE



  while (1)
  {
	delayMs(1000);
	GPIOA->ODR ^= 1<<5;
  }
}

void delayMs(int n)
{
	int i;
	SysTick->CTRL=0;               //CONTROL AND STATUS REGISTER IS CLEARED
    SysTick->VAL=0;
	SysTick->LOAD=15900;
	SysTick->CTRL =5 ;

	for(i=0;i<n;i++)
	{
		while((SysTick->CTRL & 0x10000) == 0)
		{

		}
	}

	SysTick->CTRL = 0; // stop timer

}


